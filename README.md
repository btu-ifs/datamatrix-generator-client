## A projekt

Ezen a felületen megadhatjuk a gyártási rendelés számát, amivel a backend beszűri a szükséges információkat, visszaadja a kliensnek, amivel datamatrix típusú vonalkódokat generálunk egy PDF-be.

## Hogyan kezdjünk hozzá?

Ahhoz, hogy saját gépen el tudd indítani, a következők szükségesek.

* Szükséges egy telepített [Node js](https://nodejs.org/en/).
* Telepített NPM:</br>
  `npm install npm@latest -g`
* Szükséges telepített [Git](https://git-scm.com/)
### Telepítés
1. Klónozd le a fájlokat Bitbucket-ről
2. Futtasd az `npm install` parancsot a modulok telepítéséhez
3. Futtasd az `npm install -D tailwindcss@npm:@tailwindcss/postcss7-compat postcss@^7 autoprefixer@^9` parancsot a tailwindcss telepíéséhez.
4. Futtasd az `npm start` parancsot, a React szerver indításához.

A weblap a [localhost:3000](http://localhost:3000) linken elérhető.


### Használt modulok
* Vonalkód generálás: [bwip-js](https://www.npmjs.com/package/bwip-js)
* Felület: [TailwindCSS](https://tailwindcss.com/)

#### IAL a adatokhoz: