import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import axios from 'axios';
import BarcodeGenerator from './BarcodeGenerator';

export default function ShopOrdQuery() {
  const location = useLocation();
  const [parameter, setParameter] = useState('');
  const [result, setResult] = useState('');


  useEffect(() => {
    const getData = async () => {
      try {
        const result = await axios.get(`api/v1/shipment/shoporder/${parameter}`);
        setResult(result.data);
      } catch (error) {
        console.log(error);
      }
    }
    setParameter(location.pathname.substr(1));
    console.log(location.pathname.substr(1));
    if (parameter.length > 0) {
      getData();
    }
  }, [location, parameter])

  return (
    <div className="flex flex-col mt-3 space-y-4">
      {
        result &&
        result.map((r) => {
          return <div className="border rounded-lg p-3" key={r.ORDER_NO}>
            <div className="text-xl font-semibold text-center">
              {String(r.ORDER_NO)}
            </div>
            <BarcodeGenerator text={String(r.A)} />
            <div className="text-xl font-medium text-center">
              LOT BATCH NO
            </div>
          </div>;
        })
      }
    </div>
  );
}