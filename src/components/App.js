import React from 'react';
import {
  BrowserRouter as Router,
} from "react-router-dom";

import ShopOrdQuery from './ShopOrdQuery';

function App() {

  return (
    <Router>
      <div className="flex justify-center">
        <ShopOrdQuery />
      </div>
    </Router>
  );
}

export default App;
