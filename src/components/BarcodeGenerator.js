/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import bwipjs from 'bwip-js';

export default function BarcodeGenerator(props) {
  const { text } = props;
  let canvas;
  useEffect(() => {
    if (text.length) {
      canvas = bwipjs.toCanvas(text, {
        bcid: 'datamatrix',
        text: text,
        scale: 5,
        includetext: true,
        textxalign: 'center',
        backgroundcolor: 'FFFFFF',
        textcolor: '000000'
      });
    }
  }, [text])

  return (
    <div className="">
      <canvas id={text} />
    </div>
  );
}

BarcodeGenerator.propTypes = {
  text: PropTypes.string,
};

BarcodeGenerator.defaultProps = {
  text: '',
}
